CC := gcc
CFLAGS := -std=c99 -pedantic -D_POSIX_C_SOURCE=200809 -D_DEFAULT_SOURCE
INSTBIN := /usr/bin/goblind

all: goblind

clean:
	rm -f goblind

install: goblind
	cp $< $(INSTBIN)

uninstall:
	rm -f $(INSTBIN)

goblind: goblind.c
	$(CC) $(CFLAGS) -o $@ $<
