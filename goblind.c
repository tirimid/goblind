#include <ctype.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>

#define LOGNAME "goblind"
#define DEFAULTMUL 1000

struct cliargs {
	char *cmd;
	unsigned long interval, mul;
	char const *name;
	bool unique, noterm;
};

static int daemonize(struct cliargs const *args);
static void err(char const *msg, ...);
static int shortflag(struct cliargs *out, char f, char const *pname);
static int longflag(struct cliargs *out, char const *f, char const *pname);
static int longopt(struct cliargs *out, char const *o);
static void usage(char const *pname);
static int parsecli(struct cliargs *out, int argc, char const *argv[]);
static void destroycliargs(struct cliargs *args);
static void sigh_quit(int arg);

static bool running;

int
main(int argc, char const *argv[])
{
	struct cliargs args;
	if (parsecli(&args, argc, argv))
		return 1;
	
	if (!args.interval) {
		destroycliargs(&args);
		err("--interval not specified!");
		return 1;
	}
	
	if (args.unique) {
		if (!args.name) {
			destroycliargs(&args);
			err("--unique specified without --name!");
			return 1;
		}
		
		// this is probably dangerous as no sanitization is done to
		// ensure the validity of the name.
		// oh well!
		char cmd[64];
		sprintf(cmd, "pgrep %s > /dev/null", args.name);
		if (!system(cmd)) {
			destroycliargs(&args);
			err("process with name %s already exists!", args.name);
			return 1;
		}
	}
	
	if (daemonize(&args)) {
		destroycliargs(&args);
		return 1;
	}
	syslog(LOG_INFO, "successfully daemonized");
	
	if (args.name) {
		prctl(PR_SET_NAME, args.name);
		syslog(LOG_INFO, "changed name to %s", args.name);
	}
	
	running = true;
	while (running) {
		if (system(args.cmd)) {
			syslog(LOG_ERR, "failed to execute command: %s!", args.cmd);
			
			if (!args.noterm) {
				syslog(LOG_INFO, "--noterm not specified, exiting");
				
				destroycliargs(&args);
				closelog();
				return 1;
			}
		}
		
		usleep(args.interval * (args.mul ? args.mul : DEFAULTMUL));
	}
	
	syslog(LOG_INFO, "execution done, exiting");
	
	destroycliargs(&args);
	closelog();
	return 0;
}

static int
daemonize(struct cliargs const *args)
{
	pid_t pid = fork();
	if (pid < 0) {
		err("failed first fork on daemonize!");
		return 1;
	} else if (pid > 0)
		exit(0);
	
	if (setsid() < 0)
		return 1;
	
	if ((pid = fork()) < 0)
		return 1;
	else if (pid > 0)
		exit(0);
	
	umask(0);
	
	chdir("/");
	
	for (int fd = sysconf(_SC_OPEN_MAX); fd >= 0; --fd)
		close(fd);
	
	// install quit signal handlers.
	struct sigaction sa = {
		.sa_handler = sigh_quit,
	};
	
	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGQUIT, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);
	
	openlog(args->name ? args->name : LOGNAME, LOG_NDELAY, LOG_DAEMON);
	
	return 0;
}

static void
err(char const *msg, ...)
{
	fputs("err: ", stderr);
	
	va_list va;
	va_start(va, msg);
	vfprintf(stderr, msg, va);
	va_end(va);
	
	fputc('\n', stderr);
}

static int
shortflag(struct cliargs *out, char f, char const *pname)
{
	switch (f) {
	case 'h':
		usage(pname);
		destroycliargs(out);
		exit(0);
	case 'n':
		out->noterm = true;
		break;
	case 'u':
		out->unique = true;
		break;
	default:
		err("unrecognized flag: %c!", f);
		return 1;
	}
	
	return 0;
}

static int
longflag(struct cliargs *out, char const *f, char const *pname)
{
	if (!strcmp(f, "help")) {
		usage(pname);
		destroycliargs(out);
		exit(0);
	} else if (!strcmp(f, "noterm"))
		out->noterm = true;
	else if (!strcmp(f, "unique"))
		out->unique = true;
	else {
		err("unrecognized flag: %s!", f);
		return 1;
	}
	
	return 0;
}

static int
longopt(struct cliargs *out, char const *o)
{
	size_t klen = 0;
	while (o[klen] != '=')
		++klen;
	
	char *k = strndup(o, klen);
	char const *v = &o[klen + 1];
	
	if (!strcmp(k, "interval")) {
		for (size_t i = 0, len = strlen(v); i < len; ++i) {
			if (!isdigit(v[i])) {
				err("invalid character in interval: %c!", v[i]);
				free(k);
				return 1;
			}
		}
		
		out->interval = atol(v);
		
		if (!out->interval) {
			err("invalid interval: %s!", v);
			free(k);
			return 1;
		}
	} else if (!strcmp(k, "name")) {
		size_t len = strlen(v);
		
		if (!len) {
			err("zero length name not allowed!");
			free(k);
			return 1;
		} else if (len > 16) {
			err("name longer than 16 bytes is not allowed!");
			free(k);
			return 1;
		}
		
		out->name = v;
	} else if (!strcmp(k, "unit")) {
		if (!strcmp(v, "us"))
			out->mul = 1;
		else if (!strcmp(v, "ms"))
			out->mul = 1000;
		else if (!strcmp(v, "s"))
			out->mul = 1000000;
		else {
			err("unrecognized unit: %s!", v);
			free(k);
			return 1;
		}
	} else {
		err("unrecognized option: %s!", k);
		free(k);
		return 1;
	}
	
	free(k);
	return 0;
}

static void
usage(char const *pname)
{
	printf("usage:\n"
	       "\t%s [options] command ...\n"
	       "options:\n"
	       "\t--help, -h             display help information\n"
	       "\t--interval=(interval)  set execution interval\n"
	       "\t--noterm, -n           disable termination upon command error\n"
	       "\t--name=(name)          make spawned process run as name\n"
	       "\t--unit=(us/ms/s)       set unit in which interval is described\n"
	       "\t--unique, -u           require process name uniqueness\n", pname);
}

static int
parsecli(struct cliargs *out, int argc, char const *argv[])
{
	*out = (struct cliargs){
		.cmd = malloc(1),
		.interval = 0,
		.mul = 0,
		.name = NULL,
		.noterm = false,
		.unique = false,
	};
	
	int i;
	
	for (i = 1; i < argc; ++i) {
		if (!strcmp(argv[i], "--")) {
			++i;
			break;
		} else if (argv[i][0] != '-')
			break;
		
		if (argv[i][1] != '-') {
			for (int j = 1; argv[i][j]; ++j) {
				if (shortflag(out, argv[i][j], argv[0])) {
					destroycliargs(out);
					return 1;
				}
			}
		} else {
			if (strchr(argv[i], '=')) {
				if (longopt(out, argv[i] + 2)) {
					destroycliargs(out);
					return 1;
				}
			} else {
				if (longflag(out, argv[i] + 2, argv[0])) {
					destroycliargs(out);
					return 1;
				}
			}
		}
	}
	
	if (i == argc) {
		destroycliargs(out);
		err("no command specified to execute!");
		return 1;
	}
	
	size_t len = 0;
	for (; i < argc; ++i) {
		len += strlen(argv[i]) + 1;
		out->cmd = realloc(out->cmd, len + 1);
		sprintf(out->cmd, "%s %s", out->cmd, argv[i]);
	}
	
	return 0;
}

static void
destroycliargs(struct cliargs *args)
{
	free(args->cmd);
}

static void
sigh_quit(int arg)
{
	running = false;
}
