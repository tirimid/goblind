# goblind

## Introduction
goblind, or (Goblin) (D)aemon, is a fairly minimalist program for executing
commands at fixed intervals. This program fills a different niche than, and
cannot be used as a replacement for, crond. Crond gives users the ability to
automate occasional tasks that occur at long, regular intervals. Goblind does
the same thing but for short intervals, allowing microsecond control for
interval duration.

## Dependencies
Software / system dependencies are:
* A Linux system
* `pgrep` command on system

## Management
* To build the program, run `make`
* To install the program, run `make install`
* To uninstall the program from the system, run `make uninstall`

## Usage
After installation, to run the command `echo hi world hello hai` using goblind
every ~7 seconds, run:

```
$ goblind --interval=7 --unit=s echo hi world hello hai
```

If you only want this program to ever be running from a single process, e.g. so
that multiple invocation of `goblind` will not start this process if it already
exists, use this instead:

```
$ goblind --interval=7 --unit=s --unique --name=echoer echo hi world hello hai
```

... which will terminate goblind if a process called "echoer" already exists.
Keep in mind that this applies for ALL processes whose names goblind can find,
not just those started by goblind itself.

For more information, run:

```
$ goblind --help
```

## Contributing
I am not accepting other people's pull requests. Feel free to fork this project
and make your own version.
